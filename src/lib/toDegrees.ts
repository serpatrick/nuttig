export default function (value: number) {
  return (value * 180) / Math.PI;
}
