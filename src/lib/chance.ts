import random from './random.js';

export default function (value: number) {
  return random(100) < value;
}
