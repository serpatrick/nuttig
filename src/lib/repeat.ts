export default function (
  value: number,
  callback: (index: number) => void,
  step = 1
) {
  for (let i = 0; i < value; i += step) {
    callback(i);
  }
}
